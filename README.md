# PHATTO
PHATTO is an atto* PHP server.

## Install

```
composer create-project supremacia/phatto
```

## Run

```
cd phatto/public
php -S localhost:80
```
Open ```http://localhost``` in your browser.

See this running at https://phatto.ga (soon we will have the manual, a chat, push messages ... to connect the users :)

*Att.: A ServiceWorker will be registered in your browser and the application will be cached locally. It is also possible, if installed on a host with "https", the installation as an Android application (smartphone).
The intent is to provide the basis for a PWA application (such as an app with notification (push), cache, etc.)*

**More information coming soon!**

---
***Atto** (symbol **a**) is a unit prefix in the metric system denoting a factor of 10−18 ... (https://en.wikipedia.org/wiki/Atto-)
